FROM registry.gitlab.com/allure-ai/docker-images/flask-gunicorn:latest

RUN apt-get update \
    && apt-get install -y git python3-dev gcc \
    build-essential \
    cmake \
    git \
    wget \
    unzip \
    yasm \
    pkg-config \
    libswscale-dev \
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libavformat-dev \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
RUN pip install --upgrade -r /requirements.txt
